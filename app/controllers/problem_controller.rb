class ProblemController < ApplicationController

	before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
	before_action :find_problem, except: [:index, :new, :create]

	def index
		@problems = Problem.all
	end

	def show
	end

	def new
		unless user_is_care?
			redirect_to root_path
		end

		@problem = Problem.new
	end

	def create
		@problem = Problem.new( problem_params )

		if @problem.save
			redirect_to @problem
		else
			render 'new'
		end
	end

	def edit
		unless user_is_care?
			redirect_to root_path
		end
	end

	def update
		unless user_is_care?
			redirect_to root_path
		end

		@problem.update(problem_params)
		redirect_to @problem
	end

	def destroy
		unless user_is_care?
			redirect_to root_path
		end

		@problem.destroy
		redirect_to root_path
	end

	private

	def problem_params
		params.require(:problem).permit(:name, :problemtype_id)
	end

	def find_problem
		@problem = Problem.find(params[:id])
	end

	def user_is_care?
		return current_user.usertype_id == 2
	end

end

class ProblemtypeController < ApplicationController

	before_action :authenticate_user!, only: [:edit, :new, :create, :update, :destroy]
	before_action :find_problemtype, except: [:index, :new, :create]

	def index
		@problemtypes = Problemtype.all	
	end

	def show
	end

	def new
		unless user_is_care?
			redirect_to root_path
		end

		@problemtype = Problemtype.new
	end

	def create
		unless user_is_care?
			redirect_to root_path
		end

		@problemtype = Problemtype.new(problemtype_params)

		if @problemtype.save
			redirect_to @problemtype
		else
			render 'new'
		end
	end

	def edit
		unless user_is_care?
			redirect_to root_path
		end
	end

	def update
		unless user_is_care?
			redirect_to root_path
		end

		if @problemtype.update( problemtype_params )
			redirect_to @problemtype
		else
			render 'edit'
		end
	end

	def destroy
		unless user_is_care?
			redirect_to root_path
		end

		@problemtype.destroy
		redirect_to root_path
	end

	private

	def problemtype_params
		params.require(:problemtype).permit(:name, :general_information)
	end

	def find_problemtype
		@problemtype = Problemtype.find( params[:id] )
	end

	def user_is_care?
		return current_user.usertype_id == 2
	end

end

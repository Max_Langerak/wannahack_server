class PatientsController < ApplicationController

    before_action :authenticate_user!
    before_action :find_patient, only: [:show, :edit, :update]

    def index
        @user_is_patient = false
        @user_is_care    = false

        if user_is_patient?
            @user_is_patient = true
            if current_user.patients.first == nil
                @patient = Patient.where("email" => current_user.email).first
                if @patient != nil
                    @patient.user_id = current_user.id
                    @patient.save
                    redirect_to patient_path(@patient)
                end
            else
                @patient = current_user.patients.first
                redirect_to patient_path(@patient)
            end
        elsif user_is_care?
            @user_is_care = true
            #care -> show patients
            #change this so that it onlys shows doctors' patient
            clients       = Client.where("user_id" => current_user.id)
            @patients   ||= []

            clients.each do |c|
                patient = Patient.find( c.patient_id )
                @patients << patient
            end
        end
    end

    def show
        @user_is_care    = false
        @user_is_patient = false
        if user_is_care?
            @user_is_care = true
            @problems = @patient.problems
            found = false
            current_user.clients.each do |c|
                if @patient.id == c.patient_id
                    found = true
                    break
                end
                unless found == true
                    redirect_to root_path
                end
            end
        elsif user_is_patient?
            @user_is_patient = true
            unless @patient.user_id == current_user.id
                redirect_to root_path
            end
        end
    end

    def new
        @patient = Patient.new
    end

    def create
        @patient = Patient.new(patient_params)
        existing_patient = Patient.where("bsn" => @patient.bsn).first

        if existing_patient == nil
            #patient does not exist
            if @patient.save
                #now get the patient and make him a client
                c            = Client.new
                c.user_id    = current_user.id
                c.patient_id = @patient.id

                redirect_to @patient
            else
                redirect_to root_path
            end
        else
            #generate a new client and add to user
            c            = Client.new
            c.user_id    = current_user.id
            c.patient_id = existing_patient.id
            c.save

            redirect_to patient_path( c.patient_id )
        end
    end

    def edit
        if user_is_care?
            found = false
            current_user.clients.each do |c|
                if c.patient_id == @patient.id
                    found = true
                    break
                end
            end
            unless found == true
                redirect_to root_path
            end
        elsif user_is_patient?
            unless @patient.user_id == current_user.id
                redirect_to root_path
            end
        end
    end

    def update
        if @patient.update( patient_params )
            redirect_to @patient
        else
            render 'edit'
        end
    end

    private

    def patient_params
        params.require(:patient).permit([:bsn, :email, :address, :mobile_phone, :home_phone, :phone_id, :user_id])
    end

    def find_patient
        @patient = Patient.find( params[:id] )
    end

    def user_is_patient?
        return current_user.usertype.id == 1
    end

    def user_is_care?
        return current_user.usertype_id == 2
    end
    
end

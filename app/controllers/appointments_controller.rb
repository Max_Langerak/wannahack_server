class AppointmentsController < ApplicationController

    before_action :authenticate_user!

    def index
        if user_is_care?
            @appointments = Appointment.where("user_id" => current_user.id)
        elsif user_is_patient?
            @appointments = Appointment,where("patient_id" => current_user.patients.first.id)
        end
    end

    def show
    end

    def new
        @appointment = Appointment.new
    end

    def create
        @appointment = Appointment.new(appointment_params)

        if @appointment.save
           redirect_to @appointment
        else
           render 'new'
        end
    end

    def edit
    end
    
    def update
       if @appointment.update(appointment_params)
            redirect_to @appointment
       else
          render 'edit'
       end
    end

    def destroy
       authenticate_care!

       @appointment.destroy
       redirect_to root_path
    end

    private

    def find_appointment
        @appointment = Appointment.find(params[:id])
    end

    def appointment_params
        params.require(:appointment).permit(:patient_id, :user_id, :when)
    end

    def user_is_care?
       return current_user.usertype_id == 2
    end

    def user_is_patient?
       return current_user.usertype_id == 1
    end

    def authenticate_care!
        unless user_is_care?
           redirect_to root_path
        end
    end

end

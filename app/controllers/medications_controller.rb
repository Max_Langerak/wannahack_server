class MedicationsController < ApplicationController
    
    before_action :authenticate_user!, except: [:index, :show]
    before_action :find_medication, except: [:index, :new, :create]

    def index
       @medications = Medication.all
    end

    def show
    end

    def new
       authenticate_care!

       @medication = Medication.new
    end
    
    def create
       authenticate_care!
       @medication = Medication.new(medication_params)

       if @medication.save
          redirect_to @medication
       else
          render 'new'
       end
    end
    
    def edit
       authenticate_care!
    end

    def update
       authenticate_care!

       if @medication.update(medication_params)
          redirect_to @medication
       else
          render 'edit'
       end
    end

    def destroy
       authenticate_care!

       @medication.destroy
       redirect_to root_path
    end

    private

    def find_medication
        @medication = Medication.find( params[:id])
    end

    def medication_params
        params.require(:medication).permit(:name)
    end 

    def user_is_care?
       return current_user.usertype_id == 2
    end

    def authenticate_care!
        unless user_is_care?
           redirect_to root_path
        end
    end

end

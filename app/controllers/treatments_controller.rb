class TreatmentsController < ApplicationController

	before_action :authenticate_user!, except: [:index, :show]
	before_action :find_treatment, except: [:index, :new, :create]

	def index
		@treatments = Treatment.all
	end

	def show
	end

	def new
		authenticate_care!

		@treatment = Treatment.new
	end

	def create
		authenticate_care!
		@treatment = Treatment.new(treatment_params)

		if @treatment.save
			redirect_to @treatment
		else
			render 'new'
		end
	end

	def edit
		authenticate_care!
	end

	def update
		authenticate_care!

		if @treatment.update(treatment_params)
			redirect_to @treatment
		else
			render 'edit'
		end
	end

	def destroy
		authenticate_care!

		@treatment.destroy
		redirect_to root_path
	end

	private

	def find_treatment
		@treatment = Treatment.find(params[:id])
	end

	def treatment_params
		params.require(:treatment).permit(:name, :general_information)
	end

	def authenticate_care!
		unless user_is_care?
			redirect_to root_path
		end
	end

	def user_is_care?
		return current_user.usertype_id == 2
	end

end

class Exclude < ActiveRecord::Base
    belongs_to :medication
    has_many :conflicts
end

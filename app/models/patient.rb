class Patient < ActiveRecord::Base
   belongs_to :user
   has_many :phones
   has_many :problems
   has_many :clients
   has_many :patient_medication
   has_many :appointments
end

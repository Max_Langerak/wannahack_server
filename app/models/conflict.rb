class Conflict < ActiveRecord::Base
    belongs_to :exclude
    belongs_to :medication
end

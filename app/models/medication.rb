class Medication < ActiveRecord::Base
   has_many :patient_medication
   has_many :excludes
   has_many :conflicts
end

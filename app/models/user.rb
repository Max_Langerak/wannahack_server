class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
	belongs_to :usertype
	has_many :patients
	has_many :clients
    has_many :appointments
    has_many :notifications
end

class Problemtype < ActiveRecord::Base
   has_many :problemtype_treatment
   has_many :problems
end

class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :user_id
      t.timestamp :when

      t.timestamps null: false
    end
  end
end

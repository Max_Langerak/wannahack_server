class CreateProblemtypeTreatments < ActiveRecord::Migration
  def change
    create_table :problemtype_treatments do |t|
      t.integer :problemtype_id
      t.integer :treatment_id

      t.timestamps null: false
    end
  end
end

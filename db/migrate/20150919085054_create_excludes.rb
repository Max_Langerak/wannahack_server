class CreateExcludes < ActiveRecord::Migration
  def change
    create_table :excludes do |t|
      t.integer :medication_id

      t.timestamps null: false
    end
  end
end

class CreateConflicts < ActiveRecord::Migration
  def change
    create_table :conflicts do |t|
      t.integer :medication_id
      t.integer :exclude_id

      t.timestamps null: false
    end
  end
end

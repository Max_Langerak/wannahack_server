class CreateTreatments < ActiveRecord::Migration
  def change
    create_table :treatments do |t|
      t.string :name
      t.text :general_information

      t.timestamps null: false
    end
  end
end

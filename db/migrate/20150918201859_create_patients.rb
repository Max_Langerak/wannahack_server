class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :bsn
      t.string :address
      t.string :mobile_phone
      t.string :home_phone
      t.integer :phone_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end

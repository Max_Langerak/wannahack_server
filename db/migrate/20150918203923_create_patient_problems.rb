class CreatePatientProblems < ActiveRecord::Migration
  def change
    create_table :patient_problems do |t|
      t.integer :patient_id
      t.integer :problem_id

      t.timestamps null: false
    end
  end
end
